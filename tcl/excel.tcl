# -*- mode: Tcl; tcl-indent-level :  2 -*-
#/////////////////////////////////////////////////////////////////////////////
# FILE    : excel.tcl
# PURPOSE : Set of functions to interact with excel's spreadsheets
#/////////////////////////////////////////////////////////////////////////////


#/////////////////////////////////////////////////////////////////////////////
namespace eval ::excel {
#
# PURPOSE: Main Namespace definition
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  #if { [string equal $::tcl_platform(platform) "windows"] } {
  #  if { $::tcl_platform(pointerSize) eq 8 } {
  #    package require twapi
  #    set data(pkg) twapi
  #  } else {
  #    package require tcom
  #    set data(pkg) tcom
  #  }
  #}

  set data(CID) {A B C D E F G H I J K L M N O Q R S T U V W X Y Z}
  foreach L1  {A B C D E F G H I J K L M N O Q R S T U V W X Y Z} {
    foreach L2  {A B C D E F G H I J K L M N O Q R S T U V W X Y Z} {
      lappend data(CID) $L1$L2
    }
  }
  set data(app) {}
}

proc ::excel::use { pkg } {
  variable data
  package require $pkg
  set data(pkg) $pkg
}

proc ::excel::getCellID { row col } {
  variable data

  return "[lindex $data(CID) [expr $col-1]]$row"
}

proc ::excel::runMacro { macroname {pars ""} } {
  variable data
  if {$pars != ""} {
    $data(app) Run $macroname $pars
   } else {
    $data(app) Run $macroname
   }
}

proc ::excel::parseCellID { corner } {
  variable data

  if {![regexp {^([[:alpha:]]+)([[:digit:]]+)$} $corner => col row]} {
    ::sadd::errorMsg "readConstraintsSystem : invalid cell identifier '$corner'"
    return [list]
  }
  set colAlpha [string toupper $col]
  set icol [expr [lsearch $data(CID) $colAlpha]+1]
  if {$icol == 0} {
    ::sadd::errorMsg "readConstraintsSystem : invalid column identifier '$colAlpha'"
    return [list]
  }
  list $row $icol
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::Open { visible } {
#
# PURPOSE: Open Excel application
# PARAMETERS: visible -> application visible or not
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  if { $data(pkg) eq "tcom" } {
    set data(app) [::tcom::ref createobject {Excel.Application}]
  } elseif { $data(pkg) eq "twapi" } {
    set data(app) [::twapi::comobj {Excel.Application}]
  }
  $data(app) Visible [expr $visible]
  $data(app) DisplayAlerts 0
  set data(wbs) [$data(app) Workbooks]
  return {ok}
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::NewWorkbook { } {
#
# PURPOSE: Create a new workbook
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  set data(wb) [$data(wbs) Add]
  set data(wss) [$data(wb) Worksheets]  
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::OpenWorkbook { path } {
#
# PURPOSE: Open an existing workbook
# PARAMETERS: path -> path of the file
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  set data(wb) [$data(wbs) Open $path]
  set data(wss) [$data(wb) Worksheets]  
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::newExcel { visible } {
#
# PURPOSE: New spreadsheet application
# PARAMETERS: visible -> application visible or not
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  if { $data(pkg) eq "tcom" } {
    set data(app) [::tcom::ref createobject {Excel.Application}]
  } elseif { $data(pkg) eq "twapi" } {
    set data(app) [::twapi::comobj {Excel.Application}]
  }
  $data(app) Visible [expr $visible]
  set data(wbs) [$data(app) Workbooks]
  set data(wb) [$data(wbs) Add]
  set data(wss) [$data(wb) Worksheets]  
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::openExcel { path visible } {
#
# PURPOSE: Open spreadsheet application
# PARAMETERS: path -> path of the file
#             visible -> application visible or not
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  if { $data(pkg) eq "tcom" } {
    set data(app) [::tcom::ref createobject {Excel.Application}]
  } elseif { $data(pkg) eq "twapi" } {
    set data(app) [::twapi::comobj {Excel.Application}]
  }
  $data(app) Visible [expr $visible]
  set data(wbs) [$data(app) Workbooks]
  set data(wb) [$data(wbs) Open $path]
  set data(wss) [$data(wb) Worksheets]  
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::openExcelInSameApp { path } {
#
# PURPOSE: Open spreadsheet application in the already existent app
# PARAMETERS: path -> path of the file
#             visible -> application visible or not
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  set data(wbs) [$data(app) Workbooks]
  set data(wb) [$data(wbs) Open $path]
  set data(wss) [$data(wb) Worksheets]  
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::nofWorksheets { } {
#
# PURPOSE: Returns the number of worksheets of the open workbook
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  return [$data(wss) Count]
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::getWorksheetName { } {
#
# PURPOSE: Returns the name of the current worksheet
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  return [$data(ws) Name]
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::getWorksheetIndex { } {
#
# PURPOSE: Returns the index of the current worksheet
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  return [$data(ws) Index]
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::addWorksheet { ws } {
#
# PURPOSE: Add a worksheet to the open workbook
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  set ref [$data(wss) Item [expr $ws]]
  set new [$data(wss) Add $ref]
  $ref Move $new
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::deleteWorksheet { ws } {
#
# PURPOSE: Delete a worksheet of the open workbook
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  set data(ws) [$data(wss) Item [expr $ws]]
  $data(ws) Delete
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::duplicateWorksheet { ws } {
#
# PURPOSE: Duplicate a worksheets of the open workbook
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  set ref [$data(wss) Item [expr $ws]]
  $ref Copy $ref
  set new [$data(wss) Item [expr $ws]]
  $ref Move $new
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::duplicateWorksheetbyname { wsname } {
#
# PURPOSE: Duplicate a worksheets of the open workbook
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  set ref [$data(wss) Item $wsname]
  $ref Copy $ref
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::moveWorksheet { ws0 wsf } {
#
# PURPOSE: Duplicate a worksheets of the open workbook
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  set ref [$data(wss) Item [expr $ws0]]
  set new [$data(wss) Item [expr $wsf]]
  $ref Move $new
  $new Move $ref 
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::getUsedRangeAddress { } {
#
# PURPOSE: Returns the used range address of the current worksheet
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  return [ [$data(ws) UsedRange] Address ]
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::selectWorksheetIndex { ws } {
#
# PURPOSE: Selects worksheet to work on
# PARAMETERS: ws -> Index of the worksheet
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  set data(ws) [$data(wss) Item [expr $ws]]
  $data(ws) Select
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::toggleHiddenWorksheetIndex { ws hid } {
#
# PURPOSE: Hide/Unhide worksheet to work on
# PARAMETERS: ws -> Index of the worksheet
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  set data(ws) [$data(wss) Item [expr $ws]]
  $data(ws) Visible $hid
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::toggleHiddenWorksheetByName { wsname  hid } {
#
# PURPOSE: Hide/Unhide worksheet to work on
# PARAMETERS: wsname -> Name of the worksheet
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  set data(ws) [$data(wss) Item [expr $wsname]]
  $data(ws) Visible $hid
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::selectWorksheetByName { wsname } {
#
# PURPOSE: Selects worksheet to work on
# PARAMETERS: wsname -> Name of the worksheet
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  set data(ws) [$data(wss) Item $wsname]
  $data(ws) Select
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::formatText { } {
#
# PURPOSE: Sets worksheet's format to text
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  set cells [$data(ws) Cells]
  #set range [$cells Range A[expr $index]]
  set select [$cells Select]
  [$data(app) Selection] NumberFormat "@"
  set range [$cells Range A1]
  set select [$range Select]
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::addRow { index } {
#
# PURPOSE: Adds row over the row index
# PARAMETERS: index -> row index
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  set cells [$data(ws) Cells]
  set range [$cells Range A[expr $index]]
  set select [$range Select]
  [[$data(app) Selection] EntireRow] Insert
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::deleteRow { index } {
#
# PURPOSE: Deletes row at the row index
# PARAMETERS: index -> row index
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  set cells [$data(ws) Cells]
  set range [$cells Range A[expr $index]]
  set select [$range Select]
  [[$data(app) Selection] EntireRow] Delete
}


#/////////////////////////////////////////////////////////////////////////////
proc ::excel::deleteRange { rango } {
#
# PURPOSE: Deletes row at the row index
# PARAMETERS: index -> row index
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  set cells [$data(ws) Cells]
  set range [$cells Range $rango]
  set select [$range Select]
  [[$data(app) Selection] $select] Delete
}


#/////////////////////////////////////////////////////////////////////////////
proc ::excel::readCell { row col } {
#
# PURPOSE: Sets value in row col
# PARAMETERS: row -> row index
#             col -> col index (number starting at 1)
#             value -> value
#
#/////////////////////////////////////////////////////////////////////////////
  variable data

  set cells [$data(ws) Cells]
  [$cells Item [expr $row] [expr $col]] Value
}

#/////////////////////////////////////////////////////////////////////////////

proc ::excel::readCell2 { row col } {
  variable data
  set cells [$data(ws) Cells]
  [$cells Item [expr $row] [expr $col]] Text
}
proc ::excel::readCell3 { row col } {
  variable data
  set cells [$data(ws) Cells]
  [$cells Item [expr $row] [expr $col]] NumberFormat
}
proc ::excel::readCellProperty { row col pro } {
  variable data
  [[$data(ws) Cells] Item [expr $row] [expr $col]] $pro
}

proc ::excel::getLastCellRow { } {
  variable data
  set cells [$data(ws) Cells]
  set r [[$cells SpecialCells 11] Row]
  set c [[$cells SpecialCells 11] Column]
  # list $r $c
  return [getCellID $r $c]
}

proc ::excel::getLastCellRow2 { } {
  variable data
  set cells [$data(ws) Cells]
  set r [[$cells SpecialCells 11] Row]
  set c [[$cells SpecialCells 11] Column]
  list $r $c
}

proc ::excel::getName { } {
  variable data
  return [$data(wb) Name]
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::cell { row col value } {
#
# PURPOSE: Sets value in row col
# PARAMETERS: row -> row index
#             col -> col index (number starting at 1)
#             value -> value
#
#/////////////////////////////////////////////////////////////////////////////
  variable data

  set cells [$data(ws) Cells]
  $cells Item [expr $row] [expr $col] $value
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::TclLst2TolSetNoQuotes {lst args} {
#
# PURPOSE: returns a TOL code that when is evaluated creates a set equivalent
#          to a tcl list.
# ARGUMETS:
#   lst  -> List to convert to TOL set (code)
#   args  -> Function's arguments as pairs "-key value"
#    -level  : Number of sublist levels to transform in TOL Set
#    -grammar: Gramatica de los elementos del ultimo nivel
#    -struct : Name of struct
#
#/////////////////////////////////////////////////////////////////////////////
  array set opt [list \
    -level   1 \
    -grammar Text \
    -struct  ""
  ]
  array set opt $args
  
  # name of struct ?
  if {[string length $opt(-struct)]} {
    set struct $opt(-struct)
  } else {
    switch -- $opt(-grammar) {
      Real     { set struct SetOfReal }
      Date     { set struct SetOfDate }
      default  { set struct SetOfText }
    }
  }  
  if {![llength $lst]} {
    return "Copy(Empty)"
  }
  if [expr $opt(-level) <= 1] {
    #set lst2 
    foreach ite $lst {
      set ite2 [string map {\" '} $ite]
      lappend lst2 $ite2
    }
#    set tol_cmd "Text WriteLn(\"original: [join $lst ,]\"); Text WriteLn(\"changed: [join $lst2 ,]\")"
#    ::tol::console eval $tol_cmd
    switch -- $opt(-grammar) {
      Real     { set code "${struct}([join $lst2 ,])" }
      Date     { set code "${struct}([join $lst2 {,}])" }
      default  { set code "${struct}(\"[join $lst2 \",\"]\")" }
    }
  } else {
    set sSet ""
    foreach ite $lst {
      lappend sSet [TclLst2TolSetNoQuotes $ite \
        -level [expr $opt(-level)-1] \
        -grammar $opt(-grammar)      \
        -struct  $opt(-struct) ]
    }
    set code "SetOfSet([join $sSet ,])"
  }
  return $code
}


#/////////////////////////////////////////////////////////////////////////////
proc ::excel::readRange { cell1 { cell2  ""} } {
#
# PURPOSE: Returns values between given range
# PARAMETERS: cell1 -> Top-left cell (excel's like cell) Ex: A1
#             cell2 -> Bottom-right cell (excel's like cell) Ex: C3
# RETURN: Value in row/col
#
#/////////////////////////////////////////////////////////////////////////////
  variable data

  set cells [$data(ws) Cells]
  if { $cell2 != "" && $cell2 != $cell1 } {
    set range [$cells Range $cell1 $cell2]
    set res [$range Value]
  } else {
    set range [$cells Range $cell1 $cell1]
    set res [$range Value]
    set res [list [list $res]]
  }
  return $res
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::row { row values } {
#
# PURPOSE: Sets values in row
# PARAMETERS: row -> row index
#             values -> list of values
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  set i 1
  foreach value $values {
    ::excel::cell $row $i $value
    incr i
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::changeWorksheetName { name } {
#
# PURPOSE: Changes name of the active worksheet
# PARAMETERS: name -> name
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  $data(ws) Name $name
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::Save { {path ""} {mode 56} } {
#
# PURPOSE: Saves workbook in the given path
# PARAMETERS: path -> path to save the file
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  if { $path == "" } {
    $data(app) Save
  } else {
    $data(app) DisplayAlerts False
    $data(wb) SaveAs $path $mode
    $data(app) DisplayAlerts True
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc ::excel::Quit { } {
#
# PURPOSE: Closes active document
#
#/////////////////////////////////////////////////////////////////////////////
  variable data
  
  if { $data(app) == "" } {
  } else {
    $data(app) Quit
    set data(app) {}
  }
}

proc ::excel::rightcell { row col } {
  variable data
  set cells [$data(ws) Cells]
  set cell  [$cells Item [expr $row] [expr $col]]
  set rcell [$cell End 2]
  return [$rcell Address]
}

proc ::excel::formatTextRange {rango format} {
#
# PURPOSE: Sets worksheet's format to text
#
#///////////////////////////////////////////////////////////////////////////// 
  variable data
  set cells [$data(ws) Cells]
  set range [$cells Range $rango ]
  set select [$range Select]
  [$data(app) Selection] NumberFormat $format

}

proc ::excel::getNumberFormatLocalGeneral { } {
  variable data
  set ws [$data(wss) Add]
  set cell1 [[$ws Cells] Item [expr 1] [expr 1]]
  set general unread
  set general [$cell1 NumberFormatLocal]
  $ws Delete
  return $general
}

proc ::excel::saveTBL { filename } {
  variable data
  # se copia la hoja
  $data(ws) Copy
  set wb [$data(app) ActiveWorkbook]
  set ws [$wb ActiveSheet]
  # se desprotege si lo estuviera
  $ws Unprotect
  # se copian y pegan las celdas sustituyendo expresiones por sus valores
  set cells [$ws Cells]
  $cells Copy
  $cells PasteSpecial [expr -4163]  
  # se reemplazan los caracteres que involucran un entrecomillado
  $cells Replace [format %c 92] [format %c%c 92 92]
  $cells Replace [format %c 9] {\t}
  $cells Replace [format %c 10] {\n}
  $cells Replace {`} {\`}
  $cells Replace [format %c 34] {``}
  $cells Replace {:} {\:}
  $cells Replace {�} {\�}
  $cells Replace {,} {��}
  $cells Replace {^} {\^}
  $cells Replace {;} {^^}  
  set read [::excel::getNumberFormatLocalGeneral]
  $cells NumberFormatLocal "\: $read"  
  $wb SaveAs $filename -4158
  $wb Close
}
