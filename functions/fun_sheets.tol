
//////////////////////////////////////////////////////////////////////////////
Text _WinPath(Text filename) 
//////////////////////////////////////////////////////////////////////////////
{ Replace(Replace(filename, "/", "\\"), "\\\\", "\\") };

//////////////////////////////////////////////////////////////////////////////
Real SaveCSVSheets(Set book, Text filename)
//////////////////////////////////////////////////////////////////////////////
{
  Real If(OSDirExist(filename<<".sheets"), OSDirRemove(filename<<".sheets"));
  Real If(OSDirExist(filename<<".sheets"), {
    WriteLn("Folder '"<<filename<<".sheets' cannot be removed.", "E");
  0}, {
    Real MkDir(filename<<".sheets");
    Set For(1, Card(book), Real (Real s) {
      Text name = Name(book[s]);
      SaveCSV(book[s], filename<<".sheets/"<<s<<"."<<name<<".tcsv", [[
        Real _.textAsFormula = True // para evitar conversiones
      ]])
    });
  1})
};

//////////////////////////////////////////////////////////////////////////////
Text _FindCSVSheet(Text filepath, Real index) 
//////////////////////////////////////////////////////////////////////////////
{
  Set sel = Select(GetDir(filepath)[1], Real (Text f) {
    TextMatch(f, ""<<index<<".*.tcsv")
  });
  If(Card(sel)==1, sel[1], "")
};

//////////////////////////////////////////////////////////////////////////////
Real FromCSVSheets(Text filename) 
//////////////////////////////////////////////////////////////////////////////
{
  Text xlsExtension = ToLower(GetFileExtension(filename));
  Real xlsMode = Case(xlsExtension=="xls", 56, // xlExcel8
    xlsExtension=="xlsx", 51, // xlOpenXMLWorkbook
    xlsExtension=="xlsm", 52, // xlOpenXMLWorkbookMacroEnabled
    True, ?);
  Real If(IsUnknown(xlsMode), {
    WriteLn("Unimplemented Excel extension: "<<xlsExtension<<".", "E");  
  0}, {
    //--------------------------------------------------------------------------
    // Preparación
    Text whileExpr = "";
    Real ii = 1;
    Text csvFile = _FindCSVSheet(filename<<".sheets", ii);
    Real While(And(csvFile!="", Real ii<=10), {
      Real If(ii==1, {
        Text whileExpr := whileExpr << "
          set wb [$wbs Open {"<<_WinPath(filename<<".sheets"<<"\\"<<csvFile)<<"} 0 [expr 1] [expr 6] [expr 0] [expr 0] [expr 1] [expr 2] {,} ]
          set ws [$wb ActiveSheet]
          [$ws Cells] Copy
          [$ws Cells] PasteSpecial [expr -4163]
          $ws Name {"<<TextSub(csvFile, TextFind(csvFile, ".")+1, -6)<<"}
          $wb SaveAs {"<<_WinPath(filename)<<"} 56
        "; // 56 es xls, 51 es xlsx
      1}, {
        Text whileExpr := whileExpr << "
          set wb2 [$wbs Open {"<<_WinPath(filename<<".sheets"<<"\\"<<csvFile)<<"} 0 [expr 1] [expr 6] [expr 0] [expr 0] [expr 1] [expr 2] {,} ]
          set wsn [[$wb Sheets] Item [expr "<<(Real ii-1)<<"]]
          set wsu [[$wb2 Sheets] Item [expr 1]]
          $wsu Move $wsn  
          set wsn {}  
          set wsu {}  
          set wb2 {}    
          set wsm [[$wb Sheets] Item [expr "<<(Real ii-1)<<"]]
          [$wsm Cells] Copy
          [$wsm Cells] PasteSpecial [expr -4163]        
          $wsm Name {"<<TextSub(csvFile, TextFind(csvFile, ".")+1, -6)<<"}
          set wsn [[$wb Sheets] Item [expr "<<(Real ii)<<"]]
          $wsn Move $wsm  
          set wsn {}     
          set wsm {}      
        ";
      1});
      Real ii := ii + 1;
      Text csvFile := _FindCSVSheet(filename<<".sheets", ii);
    1});
    Real If(ii>2, {
      Text whileExpr := whileExpr << "
        set ws1 [[$wb Sheets] Item [expr 1]]
        $ws1 Select    
        set ws1 {}    
        $wb Save
        $wb Close     
        set wb {}
      ";
    1}, {
      Text whileExpr := whileExpr << "
        $wb Close 
        set wb {}
      ";
    1});
    Text tclExpr = "
      proc ::excel::doscript { } {
        variable data
        set wbs $data(wbs)
        "<<whileExpr<<"
        return {ok}
      }
      ::excel::doscript
    ";
    //--------------------------------------------------------------------------
    // Ejecución
    Text API::XLSQuit(?); // No puede abrirse otro sin cerrar el anterior.
    Set pids0 = _ExcelPIDs(?);
    Text ans = _TclEvalT("::excel::Open 0");
    If(ans=="ok", {
      Set pids1 = _ExcelPIDs(?);
      Set pids = pids1-pids0;
      Real If(Card(pids)==1, {    
        Real _CurrentExcelPID := pids[1];
        WriteLn("Excel PID: "<<_CurrentExcelPID, "U");      
      1}, {
        WriteLn("Unknown Excel PID. "<<Card(pids)
          <<" new processes were found.", "U");
      0});
      Text _TclEvalT(tclExpr);
      Text API::XLSQuit(?);
    1}, {
      WriteLn("Excel.Application cannot be opened.", "E");
    0})
  })
};